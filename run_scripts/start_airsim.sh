#!/bin/bash
# Determine dirname of this file/directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SESSION="airsim"
ENV="LandscapeMountains" # e.g. 'LandscapeMountains', 'Forest'

# Docker cleanup on exit
trap "{
        docker container kill airsim_ue4
        echo Stopping TMUX Session;
        tmux kill-session -t ${SESSION};
        exit 0; 
      }" EXIT

echo "$(date) Starting ${SESSION}"
tmux new-session -d -s $SESSION

i=0

echo "Starting Unreal Engine."
tmux select-window -t $SESSION:${i}
tmux rename-window -t $SESSION:${i} 'AirSimEngine'
tmux send-keys "$DIR/docker/airsim_ue4_docker_shell.sh" C-m
tmux send-keys "/home/airsim_user/environments/$ENV/$ENV.sh -windowed -ResX=1080 -ResY=720 -opengl4" C-m
i=$((i+1))

echo "Running, press C^c to exit..."

sleep infinity
