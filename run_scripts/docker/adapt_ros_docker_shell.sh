DOCKER_IMAGE_NAME=adapt_ros
container_name=${1-"adapt_ros"}

# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Create Image
docker run -it \
    --gpus all \
    --net host \
    --rm \
    --name $container_name \
    $DOCKER_IMAGE_NAME \
    /bin/bash
