DOCKER_IMAGE_NAME=airsim_ue4
container_name=${1-"airsim_ue4"}

# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
AIRSIM="$DIR/../../AirSim"

# Open up xhost (for display) only to the container.
xhost +local:`docker inspect --format='{{ .Config.Hostname }}' $container_name`

# now, let's mount the user directory which points to the unreal binary (UNREAL_BINARY_PATH)
# set the environment varible SDL_VIDEODRIVER to SDL_VIDEODRIVER_VALUE
# and tell the docker container to execute UNREAL_BINARY_COMMAND
docker run -it \
    --gpus all \
    --user $(id -u):$(id -g) \
    -e "HOME=/home/airsim_user" \
    -e "DISPLAY=$DISPLAY" \
    -e "QT_X11_NO_MITSHM=1" \
    -e "SDL_VIDEODRIVER=$SDL_VIDEODRIVER_VALUE" \
    -e "SDL_HINT_CUDA_DEVICE='0'" \
    -v "/etc/group:/etc/group:ro" \
    -v "/etc/passwd:/etc/passwd:ro" \
    -v "/etc/shadow:/etc/shadow:ro" \
    -v "$AIRSIM/Documents:/home/airsim_user/Documents" \
    -v "$AIRSIM/environments:/home/airsim_user/environments" \
    -v "/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --net "host" \
    --privileged \
    --rm \
    --name=$container_name \
    $DOCKER_IMAGE_NAME \
    /bin/bash
