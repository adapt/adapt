#!/bin/bash
# Determine dirname of this file/directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SESSION="adapt_demo"
ENV="Forest" # e.g. 'LandscapeMountains', 'Forest'

# Docker cleanup on exit
trap "{
        docker container kill roscore
        docker container kill airsim_ros
        docker container kill adapt_ros
        docker container kill airsim_ue4
        docker container kill image_view
        echo Stopping TMUX Session;
        tmux kill-session -t ${SESSION};
        exit 0; 
      }" EXIT

echo "$(date) Starting ${SESSION}"
tmux new-session -d -s $SESSION

i=0

echo "Starting roscore."
tmux select-window -t $SESSION:${i}
tmux rename-window -t $SESSION:${i} 'Roscore'
tmux send-keys "$DIR/docker/airsim_ros_docker_shell.sh roscore" C-m
tmux send-keys "source /opt/ros/melodic/setup.bash" C-m
tmux send-keys "roscore" C-m
i=$((i+1))

sleep 1

echo "Starting Unreal Engine."
tmux new-window -t $SESSION:${i} -n "AirSimEngine"
tmux send-keys "$DIR/docker/airsim_ue4_docker_shell.sh" C-m
tmux send-keys "/home/airsim_user/environments/$ENV/$ENV.sh -windowed -ResX=1080 -ResY=720 -opengl4" C-m
i=$((i+1))

echo "Starting Airsim ROS Image Publisher."
tmux new-window -t $SESSION:${i} -n "AirSimImgPub"
tmux send-keys "$DIR/docker/airsim_ros_docker_shell.sh airsim_ros" C-m
tmux send-keys "source /opt/ros/melodic/setup.bash" C-m
tmux send-keys "source /opt/airsim_ros_ws/devel/setup.bash" C-m
tmux send-keys "roslaunch --wait airsim_ros_client pubImages.launch" C-m
i=$((i+1))

echo "Starting ROS Segmentation Node."
tmux new-window -t $SESSION:${i} -n "SegmentationNode"
tmux send-keys "$DIR/docker/adapt_ros_docker_shell.sh" C-m
tmux send-keys "source /opt/ros/melodic/setup.bash" C-m
tmux send-keys "source /opt/adapt_ros_ws/devel/setup.bash" C-m
tmux send-keys "roslaunch --wait segmentation segment.launch" C-m
i=$((i+1))

sleep 1

echo "Starting ROS Image View."
tmux new-window -t $SESSION:${i} -n "ImageView"
tmux send-keys "$DIR/docker/airsim_ros_docker_shell.sh image_view" C-m
tmux send-keys "rosrun rqt_image_view rqt_image_view /airsim/rgb/segmented" C-m
i=$((i+1))

echo "Running, press C^c to exit..."

sleep infinity
