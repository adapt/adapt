# Tutorial: ADAPT AirSim Segmentation Model

This tutorial explores the use of simulated data in the training of your very own aerial segmentation neural network! 

## Requirements

1. A computer capable of running docker. An Nvidia GPU with CUDA support is recomended. See (TODO: link to nvidia docker docs)
2. Real or simulated data with segmentation annotations. See (TODO: link to arisim tutorial) for details on how to create your own.
3. 

For those unfamiliar with Docker, see this [Docker Setup](/docker). To run this tutorial, your 


## Run the Training Container

Run the following commands to pull down and build the training docker image.

    # Clone the source directory
    git clone (airsim model training dir)
    
    # Change to docker source dir
    cd docker/
    
    # Build the docker image
    source build_docker.sh
    
    # Here, you should modify the paths within run_docker.sh to point to the src dir of the training repo, the source data, and the training output dir.
    
    # Create a container from the built image
    source run_docker.sh
    
    # Enter your docker container to begin training 
    docker exec -it adapt_dev /bin/bash
    
Or, pull down the pre-built image:

    docker pull (path to registry image TODO)


## Start Training

Inside the docker container, run the following command to begin training. The training will print the status of the training at every iteration 
(each pass of a batch of images through the network). The reported loss nuber should be going down after the first few iterations, this means 
that the model is fitting to your data and the output predicitons of the network are getting closer to the ground truth data. 

    python3 train.py --config configs/bisenet_airsim.yml


(TODO: picture of training output example)

## Tasting Your Model