# Tutorial 1: Gathering Training Data with AirSim
Requirements:
* Docker >= 19.03
* Nvidia GPU >= 2GB
* Nvidia Driver >= 440
* CUDA >= 10.0
* Free Disk Space >= 5GB
* tmux

This tutorial uses AirSim to create training data for a segmentation model. Camera images along with associated 
ground-truth depth and semantic segmentation images are rendered and saved to disk as you navigate a simulated drone 
through a forested mountain landscape. The pairing of raw imagery with ground truth allows training a semantic
segmentation deep-neural-network model using ADAPT training tools.

## Steps
1. `git clone https://gitlab.kitware.com/noaa_uav/adapt/adapt ~/adapt`
2. `cd ~/adapt/`
3. `bash ./AirSim/environments/download_forest.sh` (1.2GB Download)
4. `bash ./docker/airsim_ue4/build.sh` (this will take a while)
5. `bash ./run_scripts/start_airsim.sh`

This will create one tmux window, with the docker container you create previously running AirSim.

Fly around with WASD (controlling the rotation) and arrow keys (controlling translation). If the prompt appears: *OpenGL is deprecated, use Vulcan*, 
simply click `OK` and continue, this is a warning for Unreal Engine.

When you are ready to 
start recording, press `r`, and confirm recording has started by viewing the text in the upper-left-hand corner.
This will create a new directory of the current date and time under adapt/AirSim/Documents/AirSim/, and all
images from flight will be saved there. When you are finished recording, press `r` again.

# Tutorial 2: Running ADAPT Live on AirSim Data
Requirements:
* Docker >= 19.03
* Nvidia GPU >= 6GB
* Nvidia Driver >= 440
* CUDA >= 10.0
* Free Disk Space >= 10GB
* tmux

This tutorial explores the ADAPT software without requiring a flyable ADAPT payload. We will use [Microsoft AirSim](https://microsoft.github.io/AirSim/) as our drone sensor emulator, for both imagery and odometry, and [ROS](https://gitlab.kitware.com/noaa_uav/adapt/adapt/-/blob/dev/tutorials/doc/ros_setup.md) for our communication between processes. All processes are run inside of [Docker](https://gitlab.kitware.com/noaa_uav/adapt/adapt/-/blob/dev/tutorials/doc/docker_setup.md) containers. The simulated images are connected to the ROS ecosystem via the [AirSim ROS Client](https://gitlab.kitware.com/noaa_uav/adapt/airsim_ros_client).

## Steps
1. `git clone https://gitlab.kitware.com/noaa_uav/adapt/adapt ~/adapt`
2. `cd ~/adapt/`
3. `bash ./AirSim/environments/download_forest.sh` (1.2GB Download)
4. `bash ./docker/build_all.sh` (this will take a while)
5. `bash ./run_scripts/start_live_demo.sh`

This will create four tmux windows, each running a docker container with a different process. These processes
will open two different GUI windows, one for AirSim and one for the ROS visualization tool, `rqt_image_view`.
If the prompt appears: *OpenGL is deprecated, use Vulcan*, simply click `OK` and continue, this is a warning
for Unreal Engine. Some click-and-drag resizing of rqt_image_view may be required to properly see the segmented
images.

Fly around with WASD (controlling the rotation) and arrow keys (controlling translation), 
viewing the output live, via rqt_image_view, of our segmentation model running in ROS. The segmentation model
works best at longer distances, so flying higher into the air is a good place to start.

If you wish to view the individual processes std out, you can run "tmux a" in a separate terminal to view
the active session.

C^c in the terminal you launched the start script from (may require an `ALT^TAB`) when finished,
this will exit all running docker containers and exit the tmux session.



Both of these tutorials can be repeated but with a different environment via the `download_landscape_mountains.sh` script 
in environments, and changing line 5 of `start_live_demo.sh`, if you wish to experiment further.

