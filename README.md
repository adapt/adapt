# ADAPT: **A**utonomous **D**ata **A**cquisition and **P**rocessing **T**echnologies

ADAPT (**A**utonomous **D**ata **A**cquisition and **P**rocessing **T**echnologies) is an open-source project started by [Kitware Inc.](https://www.kitware.com/) to enable automated processing on board small unmanned aircraft systems (sUAS) to support key [NOAA](https://www.noaa.gov/) missions. Refer to the [following link](https://uas.noaa.gov/Portals/5/Docs/NOAA%20UAS%20Program%20Overview%2019Apr2019.pdf?ver=2019-04-22-144716-137) for more details on the important work that NOAA is doing to leverage sUAS for disaster forecast and response as well as wildlife and habitat monitoring. The objective of the ADAPT project is to develop a lightweight, low-power sUAS payload to provide on-board, real-time, deep-learning-based computer vision processing to autonomously distill down high-volume data into compact analytics that can be immediately transmitted to decision makers. Further objectives of the ADAPT payload include:

* Autonomous data adjudication and sensor control (e.g., visible and infrared cameras, navigation sensors) for optimized data collection during beyond-line-of-sight missions
* Automatic geo-registration of results leveraging navigation-sensor data and computer vision processing
* Curation of collected data for rapid and streamlined integration into [VIAME](https://www.viametoolkit.org/) for do-it-yourself deep-neural-network (DNN) training
* Real-time, on-board deployment of VIAME-trained deep neural networks for object (e.g., animal) detection and tracking as well as image segmentation (e.g., land cover mapping) with boundary vectorization for transmission over low-bandwidth down-links
* Support open science with easy-to-use, open-source software that can be deployed on low-cost, readily-available processing hardware, such as [NVIDA's Jetson](https://developer.nvidia.com/buy-jetson) edge computers

The ADAPT payload was conceived as the on-board data collection and processing counterpart to the desktop and web-based processing capabilities of [VIAME](https://www.viametoolkit.org/). VIAME is an open-source toolkit developed in cooperation with NOAA’s [Automated Image Analysis Strategic Initiative (AIASI)](https://www.st.nmfs.noaa.gov/aiasi/Home.html) to place tools for image and video annotation and deep-learning-based training into the hands of domain experts, such as biologists, ecologists, and meteorologists, without requiring a computer science or machine learning background. VIAME is installed and is in use at all six of the NOAA National Marine Fisheries Science Centers, allowing researchers to transition from tedious and expensive manual assessment of species health and abundance to automated computer vision analysis. Full integration of ADAPT with VIAME provides researchers with a consistent ecosystem of tools spanning the entire life cycle of their data, allowing quicker deployment of more-complete data products for their customers. For more details on how VIAME-ADAPT integration can add value to NOAA missions, see these [case studies](/doc/noaa_case_studies.md).

## Structure of ADAPT Project

At this phase of the project, we are focused on developing and refining one concrete implementation of the ADAPT payload and sensor stack with an emphasis on low-cost components, clear documentation, and tutorials so that the project is approachable and the system's utility becomes clear to a wider audience of potential users. Currently, support is focused on the [NVIDA Jetson](https://developer.nvidia.com/buy-jetson) Xavier and Nano edge computers
and the primary sensing package provided by the [ZED2 stereo camera](https://www.stereolabs.com/zed-2/).

In support of the open-source nature of this project, we leverage the [Robot Operating System (ROS)](https://www.ros.org/). ROS provides a rich ecosystem of out-of-the-box capabilities for interfacing with a wide array of cameras and sensors as well as common autonomous-system processing.

## Getting Started with ADAPT

A good entry point into exploring the ADAPT processing software without requiring a flyable ADAPT payload is provided by the tutorial: [ADAPT Processing of AirSim Data](/doc/airsim_docker.md). This tutorial uses [Microsoft AirSim](https://microsoft.github.io/AirSim/) as a drone sensor emulator running alongside ADAPT processes within a fully Dockerized environment. Camera images along with associated ground-truth depth and semantic segmentation images are rendered and saved to disk as you navigate a simulated drone with your keyboard through a forested mountain landscape. The pairing of raw imagery with ground truth allows training a semantic segmentation deep-neural-network model using ADAPT training tools. Finally, the trained model is deployed to process AirSim imagery in real time.

# License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
