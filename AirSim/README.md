# AirSim via Docker

[Microsoft AirSim](https://microsoft.github.io/AirSim/) is an open source simulator for drones and cars navigating through an environment rendered by the [Unreal Engine](https://www.unrealengine.com/). Raw imagery, truth stereo depth, and truth segmentations images are returned. This provides a useful capability for this project to generate realistic data to develop and test with without needing a functional ADAPT payload and drone.

For those unfamiliar with Docker, see this [Docker Setup](/docker). To run this tutorial, your 

# Docker

The deployed ADAPT payload is currently intended for use with [NVIDA Jetson](https://developer.nvidia.com/buy-jetson) Xavier and Nano edge computers, which currently do not support Docker. However, this directory provides dockerized versions of the ADAPT processing to support development and to allow easy execution of tutorials without a properly-provisioned NVIDIA Jetson.

## Requirements

We are currently only explicitly supporting running with Docker version 19.03.12 or greater on Ubuntu 16.04 or 18.04.

* [Install Docker](https://docs.docker.com/engine/install/ubuntu/)
* NVIDIA Driver that supports CUDA 10.2.89
