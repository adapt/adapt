# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

wget https://github.com/microsoft/AirSim/releases/download/v1.2.0Linux/Forest.zip -O $DIR/Forest.zip
unzip $DIR/Forest.zip -d $DIR
rm $DIR/Forest.zip
