# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

wget https://github.com/microsoft/AirSim/releases/download/v1.3.1-linux/LandscapeMountains.zip -O $DIR/LandscapeMountains.zip
unzip $DIR/LandscapeMountains.zip -d $DIR/
rm $DIR/LandscapeMountains.zip
