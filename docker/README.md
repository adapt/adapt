# Docker

The deployed ADAPT payload is currently intended for use with [NVIDA Jetson](https://developer.nvidia.com/buy-jetson) Xavier and Nano edge computers, which currently do not support Docker. However, this directory provides dockerized versions of the ADAPT processing to support development and to allow easy execution of tutorials without a properly-provisioned NVIDIA Jetson.

## Requirements

We are currently only explicitly supporting running with Docker version 19.03.12 or greater on Ubuntu 16.04 or 18.04.

* [Install Docker](https://docs.docker.com/engine/install/ubuntu/)
* NVIDIA Driver that supports CUDA 10.2.89
