# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

NAME=${PWD##*/}
TAG="latest"
IMAGENAME="${NAME}:${TAG}"


( docker build -t $IMAGENAME -f $DIR/Dockerfile .)
