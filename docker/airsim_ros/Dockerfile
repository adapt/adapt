FROM osrf/ros:melodic-desktop as ros_base

WORKDIR /root
# setup environment
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV ROS_DISTRO melodic
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -q -y --allow-unauthenticated \
    python-catkin-tools \
    python-pip \
    python-tk \
    python-rosdep \
    python-rosinstall \
    python-vcstools \
    dirmngr \
    gnupg2 \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y \
    ros-melodic-tf2-sensor-msgs \
    ros-melodic-mavros \
    libgl1-mesa-glx \
    libqt5x11extras5 \
    gdal-bin \
    python-gdal \
    openssh-client \
    && rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir \
    msgpack-rpc-python

# ---------------------- Install AirSim ROS Client -------------------------
FROM ros_base as airsim_client

WORKDIR /opt
SHELL ["/bin/bash", "-c"]

# copy your private ssh key for gitlab auth (https://docs.gitlab.com/ee/ssh/)
ARG SSH_PRIVATE_KEY
RUN mkdir -p ~/.ssh && umask 0077 && echo "${SSH_PRIVATE_KEY}" > ~/.ssh/id_ed25519 \
    && ssh-keyscan gitlab.kitware.com >> ~/.ssh/known_hosts

RUN mkdir -p /opt/airsim_ros_ws/src/ \
    && git clone git@gitlab.kitware.com:noaa_uav/adapt/airsim_ros_client.git \
    /opt/airsim_ros_ws/src/airsim_ros_client
WORKDIR /opt/airsim_ros_ws

RUN source /opt/ros/melodic/setup.sh \
    && catkin build --force-cmake --no-status --no-summarize --no-notify -DSETUPTOOLS_DEB_LAYOUT=OFF

# delete our private key from the image
RUN rm -R ~/.ssh

# ---------------------- Copy Build Products -------------------------
#FROM ros_base as final
#WORKDIR /opt/airsim_ros_ws
#COPY --from=airsim_client /opt/airsim_ros_ws/devel /opt/airsim_ros_ws/devel/
#COPY --from=airsim_client /opt/airsim_ros_ws/build /opt/airsim_ros_ws/build/
