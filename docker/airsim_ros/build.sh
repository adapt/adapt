# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

NAME=${PWD##*/}
TAG="latest"
IMAGENAME="${NAME}:${TAG}"

export SSH_PRIVATE_KEY="$(cat ~/.ssh/id_ed25519)"
( docker build --build-arg SSH_PRIVATE_KEY -t $IMAGENAME -f $DIR/Dockerfile .)
