#! /bin/bash
# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

# Exit if any command fails
set -e
for d in $DIR/*/; do
    if [ -f "$d/build.sh" ]; then
        bash "$d/build.sh"
    fi
done
