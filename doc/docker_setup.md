# Docker Motive

For those unfamiliar with Docker, in the context of this project, Docker allows us (the authors) to make our tutorials as easy, reproducible, and self-contained as possible. Docker minimizes the number of things that you (the user) have to install on your operating system. With Docker, you don't have to worry about cluttering your system with various libraries and packages or version mismatches.

Docker allows us to define a text template of the environment (a *Dockerfile*) we wish to create, including any programs and libraries are to be installed and the explicit versions therein. This template will always start from some well-defined starting point (e.g., base install of Ubuntu 18.04). The Dockerfile is then compiled into a Docker *image* on your system encoding the result of the OS provisioning exactly as it was defined in the Dockerfile. The `dockerd` service can then load this image (via `docker run`) to create an instance (a Docker *container*) so that processing can be run inside this static environment. In this way, the user can run software in almost the exact same environment that it was developed in, reducing the possibility for errors.

A Docker environment is lightweight and portable, and is close to the version-locking capabilities of a VM, but is dependent on the host OS. In our case, this means that hardware drivers, specifically GPU drivers, are not portable across systems via Docker.

ADAPT processing is intended to be run on lightweight, low-power NVIDIA Jetson computers, which unfortunately do not effectively support GPU use in Docker. However, Docker allows us to define a reasonable proxy environment to facilitate development and testing offboard the Jetson computers.

## Docker Environment

The Docker examples in this repository require Docker version >= 19.03.12 running on a host Ubuntu 16.04 or 18.04 operating system and a host Nvidia Driver >= 440 on a GPU with at least 4GB of memory (tested on Quadro RTX 3000 and ...).
