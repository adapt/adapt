# ROS Motive

The Robot Operating System ([ROS](https://www.ros.org/)) is an open-source collection 
of libraries and packages that functions as a middleware between the OS and processes. It is a
flexible framework, designed for full-stack robotics systems such as ADAPT.

We take advantage of several ROS functionalities in our software stack.
It serves as a message-passing service between our processes (known as `nodes`),
which can be in C++ or Python. We use its visualization and introspection tools to 
view passed messages between nodes (e.g. `rqt_image_view`, `rostopic`), and its recording
tools to share these messages (e.g.`rosbag`). 

In addition, ROS has a large selection of open-source packages implementing various
algorithms and hardware drivers, supported by its community and interested parties. The Zed 2,
our camera of choice, is supported by [StereoLabs](https://www.stereolabs.com/zed-2/) 
with a full ROS implementation from hardware drivers to 3D depth processing. [AirSim](https://microsoft.github.io/AirSim/),
our simulator of choice, also comes with a full ROS implementation from live imagery
to UAV odometry.

For the purposes of this tutorial, most critical is its superbuild system *catkin*.
This in conjunction with Docker, allows us
to keep all our software packages and installs in only one directory, the
[workspace](https://gitlab.kitware.com/noaa_uav/adapt/adapt_ros_ws). This keeps 
the project isolated from your system environment, and keeps the global changes
to an absolute minimum.

## ROS Environment

All of our testing was done with ROS Melodic.
