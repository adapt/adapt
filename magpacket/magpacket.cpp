/****************************************************************/
/*                                                              */
/*          Mag cal for Advanced Navigation's Spatial           */
/*          Copyright 2021, Kitware, Inc.                       */
/*                                                              */
/****************************************************************/
/*
 See parent project for license in use
*/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <unistd.h>
#include <vector>

#include "serial/serial.h"
#include "an_packet_protocol.h"
#include "spatial_packets.h"

#define NUM_PACKETS_TIMEOUT 500000
#define SERIAL_TIMEOUT_MS 2000

template <class T>
T fetchInput(std::string const& message)
{
    T result;
    std::cout << message;
    std::cin >> result;
    return result;
}

void listCommands()
{
   std::cout <<
    "Enter a command to start the process: \n" <<
    "    q: quit the program\n" <<
    "    s: get the status of the magnetic calibration\n" <<
    "    2: start a 2d calibration\n" <<
    "    3: start a 3d calibration\n" <<
    "    c: cancel calibration\n" <<
    "      note: to cancel you may need to clone and reopen\n";
}

void listUsage()
{
    std::cout << "usage: magpacket comPort baudrate\n";
}

void sendRequest(std::string packet, serial::Serial *ser)
{
    uint8_t packet_id = static_cast<uint8_t>(std::stoi(packet,nullptr));
    std::cout << "requesting packet: " << unsigned(packet_id) << "...\n";
    an_packet_t *anp = encode_request_packet(packet_id);
    an_packet_encode(anp);
    // send the packet
    int ret = ser->write(an_packet_pointer(anp), an_packet_size(anp));
    an_packet_free(&anp);
    if (ret == -1)
        std::cout << "Error sending request - return code: " << ret << "\n";
    return;
}

void sendCommandWithArg(std::string packet, std::string arg, serial::Serial *ser)
{
    uint8_t packet_id = static_cast<uint8_t>(std::stoi(packet,nullptr));
    uint8_t arg_val = static_cast<uint8_t>(std::stoi(arg,nullptr));
    std::cout << "sending packet: " << unsigned(packet_id) <<
        " with argument: " << unsigned(arg_val) << "\n";
    magnetic_calibration_configuration_packet_t magp;
    magp.magnetic_calibration_action = arg_val;
    an_packet_t *anp = encode_magnetic_calibration_configuration_packet(&magp);
    an_packet_encode(anp);
    // send the packet
    int ret = ser->write(an_packet_pointer(anp), an_packet_size(anp));
    an_packet_free(&anp);
    if (ret == -1)
        std::cout << "Error sending command - return code: " << ret << "\n";
    return;
}

std::string convertStatus(uint8_t packetVal)
{
    std::string stat = "";
    // status messages from Spatial Reference Manual
    switch (packetVal)
    {
        case 0:
        {
            stat = "Magnetic calibration not completed"; break;
        }
        case 1:
        {
            stat = "2D magnetic calibration completed"; break;
        }
        case 2:
        {
            stat = "3D magnetic calibration completed"; break;
        }
        case 3:
        {
            stat = "Custom values magnetic calibration completed"; break;
        }
        case 4:
        {
            stat = "unknown?"; break;
        }
        case 5:
        {
            stat = "2D calibration in progress"; break;
        }
        case 6:
        {
            stat = "3D calibration in progress"; break;
        }
        case 7:
        {
            stat = "2D calibration error: excessive roll"; break;
        }
        case 8:
        {
            stat = "2D calibration error: excessive pitch"; break;
        }
        case 9:
        {
            stat = "Calibration error: sensor over range event"; break;
        }
        case 10:
        {
            stat = "Calibration error: time-out"; break;
        }
        case 11:
        {
            stat = "Calibration error: system error"; break;
        }
        case 12:
        {
            stat = "Calibration error: interference error"; break;
        }
    }
    return stat;
}

void getMagStatInfo(magnetic_calibration_status_packet_t const *mag_stat, std::string *stat,
                    uint8_t *progress, uint8_t *percent)
{
    magnetic_calibration_status_packet_t mag = *mag_stat;
    *stat = convertStatus(mag.magnetic_calibration_status);
    std::cout << "   Status: " << *stat << "\n";

    *progress = mag.magnetic_calibration_progress_percentage;
    std::cout << "   Progress: " <<
        unsigned(*progress) << "\n";

    *percent = mag.local_magnetic_error_percentage;
    std::cout << "   Error(%): " <<
        unsigned(*percent) << "\n\n";
}

bool getCalPercent(int timeout, uint8_t *progress, serial::Serial *ser)
{
    // get data from com port //
    an_decoder_t an_decoder;
    an_packet_t *an_packet;
    int bytes_received;

    an_decoder_initialise(&an_decoder);

    bool gotResponse = false;
    int cnt = 0;
    bool stayInLoop = true;
    while(stayInLoop)
    {
        if (gotResponse) stayInLoop = false;

        // timeout
        cnt++;
        if (cnt > timeout && gotResponse==false)
        {
            stayInLoop = false;
        }

        // recv
        if ((bytes_received = ser->read(an_decoder_pointer(&an_decoder), an_decoder_size(&an_decoder))) > 0)
        {
            // increment the decode buffer length by the number of bytes received //
            an_decoder_increment(&an_decoder, bytes_received);

            // decode all the packets in the buffer //
            //while ((an_packet = an_packet_decode(&an_decoder)) != NULL)
            if ((an_packet = an_packet_decode(&an_decoder)) != NULL)
            {
                if (an_packet->id == packet_id_magnetic_calibration_status)
                {
                    magnetic_calibration_status_packet_t mag_stat;
                    decode_magnetic_calibration_status_packet(&mag_stat, an_packet);
                    uint8_t error;
                    std::string stat;
                    getMagStatInfo(&mag_stat, &stat, progress, &error);

                    gotResponse = true;
                }
                else if (an_packet->id == 20 || an_packet->id == 27 ||
                         an_packet->id == 28 || an_packet->id == 29)
                { // ignore these
                }
                else
                {
                    std::cout << "unknown packet: " << unsigned(an_packet->id) << "\n";
                }

                // Ensure that you free the an_packet when your done with it //
                // or you will leak memory                                   //
                an_packet_free(&an_packet);
            }
        }
    } // end while(stayInLoop)
    return gotResponse;
}

void loopStatus(int timeout, serial::Serial *ser)
{
    uint8_t progress = 0;
    while(progress < 100)
    {
        sendRequest("191", ser);
        getCalPercent(timeout, &progress, ser);
    }
}

bool checkAckResponse(int timeout, serial::Serial *ser)
{
    // get data from com port //
    an_decoder_t an_decoder;
    an_packet_t *an_packet;
    int bytes_received;

    an_decoder_initialise(&an_decoder);

    bool gotResponse = false;
    int cnt = 0;
    bool stayInLoop = true;
    while(stayInLoop)
    {
        if (gotResponse) stayInLoop = false;

        // timeout
        cnt++;
        if (cnt > timeout && gotResponse==false)
        {
            stayInLoop = false;
        }

        // recv
            if ((bytes_received = ser->read(an_decoder_pointer(&an_decoder), an_decoder_size(&an_decoder))) > 0)
            {
                // increment the decode buffer length by the number of bytes received //
                an_decoder_increment(&an_decoder, bytes_received);

                // decode all the packets in the buffer //
                //while ((an_packet = an_packet_decode(&an_decoder)) != NULL)
                if ((an_packet = an_packet_decode(&an_decoder)) != NULL)
                {
                    if (an_packet->id == packet_id_acknowledge)
                    {
                        acknowledge_packet_t acknowledge_packet;
                        decode_acknowledge_packet(&acknowledge_packet, an_packet);
                        printf("acknowledge id %d with result %d\n", acknowledge_packet.packet_id, acknowledge_packet.acknowledge_result);
                        gotResponse = true;
                    }
                    else if (an_packet->id == 20 || an_packet->id == 27)
                    { // ignore these
                    }
                    else
                    {
                        std::cout << "unknown packet: " << unsigned(an_packet->id) << "\n";
                    }

                    // Ensure that you free the an_packet when your done with it //
                    // or you will leak memory                                   //
                    an_packet_free(&an_packet);
                }
            }
    } // end while(stayInLoop)
    return gotResponse;
}

int main(int argc, char *argv[]) {

    // Set up the COM port
    std::string com_port;
    uint32_t baud_rate;
    std::string imu_frame_id;
    std::string nav_sat_frame_id;
    std::string topic_prefix;

    if (argc < 3)
    {
        listUsage();
        return 1;
    }

    com_port = std::string(argv[1]);
    baud_rate = atoi(argv[2]);

    // port, baudrate, timeout in milliseconds
    serial::Serial serialPort(com_port, baud_rate, serial::Timeout::simpleTimeout(SERIAL_TIMEOUT_MS));

    if (!serialPort.isOpen())
    {
        printf("Could not open serial port: %s \n",com_port.c_str());
        exit(EXIT_FAILURE);
    }

    bool handleInput = true;
    while(handleInput)
    {
        listCommands();
        std::string in = fetchInput<std::string>("enter command: ");
        char cmd[in.size() + 1];
        std::copy(in.begin(), in.end(), cmd);
        cmd[in.size()] = '\0';
        switch (cmd[0])
        {
            case 'q':
            {
                handleInput = false;
                return 0;
            }
            case 's':
            {
                bool gotResponse = false;
                sendRequest("191", &serialPort);
                uint8_t per;

                if (getCalPercent(NUM_PACKETS_TIMEOUT, &per, &serialPort) == false)
                {
                    // try sending once more:
                    sendRequest("191", &serialPort);
                    if (getCalPercent(NUM_PACKETS_TIMEOUT, &per, &serialPort) == false)
                        std::cout << "timed out waiting for response\n";
                }
                break;
            }
            case '2':
            {
                // send the command
                bool gotResponse = false;
                sendCommandWithArg("190", "2", &serialPort);
                if (checkAckResponse(NUM_PACKETS_TIMEOUT, &serialPort) == false)
                    std::cout << "timed out waiting for response\n";
                else
                    loopStatus(NUM_PACKETS_TIMEOUT, &serialPort);
                break;
            }
            case '3':
            {
                bool gotResponse = false;
                sendCommandWithArg("190", "3", &serialPort);
                if (checkAckResponse(NUM_PACKETS_TIMEOUT, &serialPort) == false)
                    std::cout << "timed out waiting for response\n";
                else
                    loopStatus(NUM_PACKETS_TIMEOUT, &serialPort);
                break;
            }
            case 'c':
            {
                bool gotResponse = false;
                sendCommandWithArg("190", "0", &serialPort);
                if (checkAckResponse(NUM_PACKETS_TIMEOUT, &serialPort) == false)
                    std::cout << "timed out waiting for response\n";
                break;
            }
        }
    }

   return 0;
}
