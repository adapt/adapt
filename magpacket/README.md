# magpacket (a magnetic calibration utility)

This tool is meant to be a *very* simple tool for performing a magnetic calibration via commandline.  This tool is based on the [advanced_navigation_driver](https://github.com/ros-drivers/advanced_navigation_driver) and uses the excellent [Serial Communication Library](https://github.com/wjwwood/serial).

# Building

Requires CMake and those required by [Serial Communication Library](https://github.com/wjwwood/serial).

```
git clone ...
git submodule init
git submodule update
cd magpacket
mkdir build && cd build
cmake ../
make
```

# Usage
```
./magpacket comPort baudrate
# for example:
./magpacket /dev/ttyUSB0 115200
```

Then follow the prompts on screen to see the status and/or perform calibrations